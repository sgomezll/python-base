from base.base_num import BaseConvert
from color.color_dec import ColorElementDec
from color.color_hex import ColorElementHex


def change_base():
    base_change = BaseConvert("A1", 16)
    base_dest = 2
    print(f"{base_change} a base {base_dest} es {base_change.change_to(base_dest).get_num()}")
    color_example = ColorElementDec(125, 12, 54)
    print(f"{color_example} en Hexadecimal es {color_example.to_hex()}")
    color_hex_example = ColorElementHex("7D0C36")
    print(f"{color_hex_example} en RGB es {color_hex_example.to_rgb()}")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    change_base()
