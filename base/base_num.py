import re


class BaseConvert:
    __MIN_BASE = 2
    __MAX_BASE = 16
    __POSSIBLE_VALUES = {
        "0": 0,
        "1": 1,
        "2": 2,
        "3": 3,
        "4": 4,
        "5": 5,
        "6": 6,
        "7": 7,
        "8": 8,
        "9": 9,
        "A": 10,
        "B": 11,
        "C": 12,
        "D": 13,
        "E": 14,
        "F": 15
    }

    def __init__(self, num, base=10) -> None:
        self.__num = self.__validate_num(num)  # El número está en el formato correcto
        self.__base = self.__validate_base(base)  # La base está en el formato correcto y además está dentro del rango
        self.__evaluate_num_base()  # Evaluamos que el número esté dentro de la base asignada
        self.__num_size = len(self.__num)

    def __validate_base(self, base) -> int:
        if not isinstance(base, int):
            raise TypeError(f"La base debe ser un entero (int) -> {base}")
        if base < self.__MIN_BASE or base > self.__MAX_BASE:
            raise ValueError(f"La base debe estar entre {self.__MIN_BASE} y {self.__MAX_BASE}")
        return int(base)

    def __validate_num(self, num) -> str:
        if not (isinstance(num, int) or isinstance(num, str)):
            raise TypeError(f"El número debe ser un entero o un string (int, str) -> {num}")
        if isinstance(num, str) and not re.compile(r"^[0-9A-Fa-f]+$").match(num):
            raise TypeError("El número no debe contener ningún caracter especial")
        str_num = str(num).upper()  # Lo convertimos finalmente en un String
        for s in str_num:
            if s not in self.__POSSIBLE_VALUES:  # Si el valor ingresado excede la base 16
                raise ValueError(f"El número {str_num} no es válido, debe estar entre las bases {self.__MIN_BASE} y "
                                 f"{self.__MAX_BASE}, símbolo inválido: {s}")
        return str_num

    def __evaluate_num_base(self):
        for s in self.__num:
            n = self.__POSSIBLE_VALUES[s]  # Comprobamos que esté en la base correcta
            if n % self.__MAX_BASE > self.__base:
                raise ValueError(f"El número {self.__num} no corresponde a la base indicada {self.__base}")

    def change_to(self, dest_base=10) -> 'BaseConvert':
        if dest_base is None:
            raise TypeError("Se debe especificar la base de destino")
        if not self.__is_in_range(dest_base):
            raise ValueError(f"El rango de la base debe estar entre {self.__MIN_BASE} y {self.__MAX_BASE}")
        if self.__base == dest_base:
            return self
        return self.__convert(dest_base)

    def __convert(self, dest_base) -> 'BaseConvert':
        possible_base_values = {key: value for key, value in self.__POSSIBLE_VALUES.items() if value < self.__base}
        possible_dest_values = {key: value for key, value in self.__POSSIBLE_VALUES.items() if value < dest_base}
        num_base_10 = self.__convert_to_base10(possible_base_values)
        if dest_base is 10:
            return BaseConvert(num_base_10, 10)
        num_base_dest = ""
        while num_base_10 > 0:
            mod = int(num_base_10 % dest_base)
            num_base_dest += list(possible_dest_values.keys())[list(possible_dest_values.values()).index(mod)]
            num_base_10 = int(num_base_10 / dest_base)
        return BaseConvert(num_base_dest[::-1], dest_base)

    def __convert_to_base10(self, possible_origin_base_values) -> int:
        origin_num = self.__num
        origin_num_size = self.__num_size - 1
        num_base_10 = 0
        for s in origin_num:
            num_base_10 += possible_origin_base_values[s] * (self.__base ** origin_num_size)
            origin_num_size -= 1
        return num_base_10

    def __is_in_range(self, dest_base) -> bool:
        return dest_base >= self.__MIN_BASE or dest_base <= self.__MAX_BASE

    def get_num(self) -> str:
        return self.__num

    def get_base(self) -> int:
        return self.__base

    def __str__(self) -> str:
        return "({0}, base: {1})".format(self.__num, self.__base)

    def __len__(self) -> int:
        return len(self.__num)
