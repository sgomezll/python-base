from base.base_num import BaseConvert


class ColorElementDec:
    __MIN_VALUE = 0
    __MAX_VALUE = 255
    __SIZE_VALUE = 2
    __TO_BASE_HEX = 16

    def __init__(self, red: int = 0, green: int = 0, blue: int = 0):
        self.__red = self.__validate(red)
        self.__green = self.__validate(green)
        self.__blue = self.__validate(blue)
        self.__red_hex = BaseConvert(self.__red).change_to(self.__TO_BASE_HEX).get_num()
        self.__green_hex = BaseConvert(self.__green).change_to(self.__TO_BASE_HEX).get_num()
        self.__blue_hex = BaseConvert(self.__blue).change_to(self.__TO_BASE_HEX).get_num()

    def __validate(self, element):
        if not isinstance(element, int):
            raise TypeError(f"El valor {element} no es de tipo int")
        if element < self.__MIN_VALUE or element > self.__MAX_VALUE:
            raise ValueError(f"El valor {element} está fuera del rango [{self.__MIN_VALUE}, {self.__MAX_VALUE}]")
        return element

    @staticmethod
    def __format_hex(color_hex):
        if len(color_hex) is 1:
            return "0" + color_hex
        return color_hex

    def get_self(self):
        return self

    def get_red(self):
        return self.__red

    def get_green(self):
        return self.__blue

    def get_blue(self):
        return self.__blue

    def to_hex(self):
        from color.color_hex import ColorElementHex
        return ColorElementHex(self.__format_hex(self.__red_hex) +
                               self.__format_hex(self.__green_hex) +
                               self.__format_hex(self.__blue_hex))

    def __str__(self):
        return f"RGB({self.__red}, {self.__green}, {self.__blue})"
