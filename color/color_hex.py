import re

from base.base_num import BaseConvert


class ColorElementHex:
    __MIN_EXP_HEX = 3
    __MAX_EXP_HEX = 6
    __BASE_DECIMAL = 10
    __BASE_HEX = 16

    def __init__(self, hex_value: str = "000000"):
        self.__value = self.__validate(hex_value)
        self.__red_hex = self.__value[0:2]
        self.__green_hex = self.__value[2:4]
        self.__blue_hex = self.__value[4:6]

    def __validate(self, hex_value):
        if not (len(hex_value) is self.__MIN_EXP_HEX or len(hex_value) is self.__MAX_EXP_HEX):
            raise ValueError(f"La longitud del valor {hex_value} debe ser {self.__MIN_EXP_HEX} o "
                             f"{self.__MAX_EXP_HEX}. La longitud del valor es {len(hex_value)}")
        if not re.compile(r"^[0-9A-Fa-f]+$").match(hex_value):
            raise TypeError(f"El valor {hex_value} contiene valores inválidos")
        hex_value = self.__format_len_3_hex(hex_value)
        if not self.__is_base_16_convertible(hex_value):
            raise ValueError(f"El valor {hex_value} no puede ser convertido, los valores deben ser entre 0 - 9 y A - F")
        return hex_value

    def __is_base_16_convertible(self, hex_value):
        try:
            self.__red = int(BaseConvert(hex_value[0:2], self.__BASE_HEX).change_to(self.__BASE_DECIMAL).get_num())
            self.__green = int(BaseConvert(hex_value[2:4], self.__BASE_HEX).change_to(self.__BASE_DECIMAL).get_num())
            self.__blue = int(BaseConvert(hex_value[4:6], self.__BASE_HEX).change_to(self.__BASE_DECIMAL).get_num())
        except TypeError or ValueError:
            return False
        return True

    def __format_len_3_hex(self, hex_value):
        if len(hex_value) is self.__MIN_EXP_HEX:
            _hex_value = ""
            for c in hex_value:
                _hex_value += (c * 2)
            hex_value = _hex_value
        return hex_value

    def get_self(self):
        return self

    def get_value(self):
        return self.__value

    def to_rgb(self):
        from color.color_dec import ColorElementDec
        return ColorElementDec(self.__red, self.__green, self.__blue)

    def __str__(self):
        return f"#{self.__value}"
